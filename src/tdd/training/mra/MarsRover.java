package tdd.training.mra;

import java.util.List;

public class MarsRover {
	private static final String ORIENTATION = "NESW";
	
	private String planet[][] = null;
	private int planetX_limit = 0;
	private int planetY_limit = 0;
	private int roverPositionX = 0;
	private int roverPositionY = 0;
	private String roverFacing = Character.toString(ORIENTATION.charAt(0));
	
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		int tmpObstaclesX = 0;
		int tmpObstaclesY = 0;
		String obstacleCoord[];
		
		
		if(planetX < 0 || planetY < 0) {
			throw new MarsRoverException("Invalid planet dimension");
		}else if(planetObstacles == null) {
			this.planetX_limit = planetX;
			this.planetY_limit = planetY;
			planet = new String[planetY][planetX];
			
		}else {
			this.planetX_limit = planetX;
			this.planetY_limit = planetY;
			planet = new String[planetY][planetX];
			
			for(String obstacle:planetObstacles) {
				obstacle = obstacle.replaceAll("[()]","").trim();
				obstacleCoord = obstacle.split(",");
				tmpObstaclesX = Integer.valueOf(obstacleCoord[0]);
				tmpObstaclesY = Integer.valueOf(obstacleCoord[1]);
				
				if(tmpObstaclesX >= planetX_limit || tmpObstaclesX < 0) {
					planet = null;
					throw new MarsRoverException("Invalid obstacle position");
				}else if(tmpObstaclesY >= planetY_limit || tmpObstaclesY < 0) {
					planet = null;
					throw new MarsRoverException("Invalid obstacle position");
				}else {
					planet[tmpObstaclesX][tmpObstaclesY] = "x";
				}
			}
		}
		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		if(x < 0 || x >= this.planetX_limit) {
			throw new MarsRoverException("Invalid obstacle position");
		}else if(y < 0 || y >= this.planetY_limit) {
			throw new MarsRoverException("Invalid obstacle position");
		}
		
		return planet[y][x].equals("x");
	}
	
	/**
	 * It compose the result string after the execution of a command
	 * @return the result string of a command
	 */
	public String composeResultString() {
		String commandResult = "";
		String x_coord = Integer.toString(this.roverPositionX);
		String y_coord = Integer.toString(this.roverPositionY);
		commandResult = commandResult.concat("("+x_coord+",");
		commandResult = commandResult.concat(y_coord+",");
		commandResult = commandResult.concat(this.roverFacing+")");
		
		return commandResult;
	}
	
	/**
	 * It manage the forward movement
	 * @throws MarsRoverException
	 */
	public void roverForwardMover() throws MarsRoverException {
		switch(this.roverFacing) {
		
		case "N":
			if(roverPositionY + 1 >= this.planetY_limit) {
				roverPositionY = 0;
			}else {
				this.roverPositionY++;
			}
			
			break;
		case "S":
			if(roverPositionY - 1 < 0) {
				roverPositionY = this.planetY_limit-1;
			}else {
				this.roverPositionY--;
			}
			
			break;
		case "E":
			if(roverPositionX + 1 >= this.planetY_limit) {
				roverPositionX = 0;
			}else {
				this.roverPositionX++;
			}
			
			break;
		case "W":
			if(roverPositionX - 1 < 0) {
				roverPositionX = this.planetX_limit-1;
			}else {
				this.roverPositionX--;
			}
			
			break;
		}
	}
	
	/**
	 * It manage the backward movement
	 * @throws MarsRoverException
	 */
	public void roverBackwardMover() throws MarsRoverException {
		switch(this.roverFacing) {
		
		case "N":
			if(roverPositionY - 1 < 0) {
				roverPositionY = this.planetY_limit-1;
			}else {
				this.roverPositionY--;
			}
			
			break;
		case "S":
			if(roverPositionY + 1 >= this.planetY_limit) {
				roverPositionY = 0;
			}else {
				this.roverPositionY++;
			}
			break;
		case "E":
			if(roverPositionX + 1 >= this.planetY_limit) {
				roverPositionX = 0;
			}else {
				this.roverPositionX++;
			}
			
			
			break;
		case "W":
			if(roverPositionX - 1 < 0) {
				roverPositionX = 0;
			}else {
				this.roverPositionX++;
			}
			
			break;
		}
	}
	
	/**
	 * It make the rover move 
	 * @param movementType type of movement(f,b)
	 * @throws MarsRoverException
	 */
	public void roverSwitchMover(String movementType) throws MarsRoverException{
		
		if(movementType.equals("f")) {
			roverForwardMover();
		}else {
			roverBackwardMover();
			}
		}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		String commandResult = "";
		int index = 0;
		int orientationIndex = ORIENTATION.indexOf(this.roverFacing);
		
		commandResult = composeResultString();
		
		for(index = 0; index < commandString.length();index ++) {
			String command = Character.toString(commandString.charAt(index));

			if(command.isEmpty()) {
				commandResult = composeResultString();
				
			}else if(command.equals("r")){
				if(orientationIndex + 1 > ORIENTATION.length() - 1 ) {
					orientationIndex = 0;
				}else {
					orientationIndex++;
				}
				this.roverFacing = Character.toString(ORIENTATION.charAt(orientationIndex));
				
				commandResult = composeResultString();
				
			}else if(command.equals("l")){
				if(orientationIndex - 1 < 0) {
					orientationIndex = ORIENTATION.length() - 1;
				}else {
					orientationIndex--;
				}
				this.roverFacing = Character.toString(ORIENTATION.charAt(orientationIndex));
				
				commandResult = composeResultString();
				
			}else if(command.equals("f")){
				 roverSwitchMover(command);
				 commandResult = composeResultString();
			}else if(command.equals("b")){
				 roverSwitchMover(command);
				 commandResult = composeResultString();
			}else {
				throw new MarsRoverException("Invalid command");
			}
			
		}
		
		return commandResult;
	}

}

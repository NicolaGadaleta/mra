package tdd.training.mra;

import static org.junit.Assert.*;
import java.util.List;
import java.util.ArrayList;
import org.junit.Test;

public class MarsRoverTest {

	@Test(expected = MarsRoverException.class)
	public void roverCreationShouldReturnAnException()throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(4,0)");
		
		MarsRover rover = new MarsRover(3,3,obstacles);
	}
	
	@Test
	public void planetContainsObstacleAtShouldReturnTrue()throws MarsRoverException {
		boolean isObstacled = false;
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(0,1)");
		obstacles.add("(2,2)");
		
		MarsRover rover = new MarsRover(3,3,obstacles);
		
		isObstacled = rover.planetContainsObstacleAt(1,0);
		
		assertTrue(isObstacled);
	}
	
	@Test(expected = MarsRoverException.class)
	public void planetContainsObstacleAtShouldReturnError()throws MarsRoverException {
		boolean isObstacled = false;
		List<String> obstacles = new ArrayList<String>();
		
		obstacles.add("(0,1)");
		obstacles.add("(2,2)");
		
		MarsRover rover = new MarsRover(3,3,obstacles);
		
		isObstacled = rover.planetContainsObstacleAt(-1,0);
		
	}
	
	@Test
	public void executeAnEmptyStringShouldReturnTheRoverPosition()throws MarsRoverException {
		String position = "";
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(0,1)");
		obstacles.add("(2,2)");
		
		MarsRover rover = new MarsRover(3,3,obstacles);
		
		position = rover.executeCommand("");
		assertEquals("(0,0,N)",position);
	}
	
	@Test
	public void turningExecutionShouldReturnTheRoverPositionButFacedEst()throws MarsRoverException {
		String position = "";
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(0,1)");
		obstacles.add("(2,2)");
		
		MarsRover rover = new MarsRover(3,3,obstacles);
		
		position = rover.executeCommand("r");
		assertEquals("(0,0,E)",position);
	}
	
	@Test
	public void turningExecutionShouldReturnTheRoverPositionButFacedOvest()throws MarsRoverException {
		String position = "";
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(0,1)");
		obstacles.add("(2,2)");
		
		MarsRover rover = new MarsRover(3,3,obstacles);
		
		position = rover.executeCommand("l");
		assertEquals("(0,0,W)",position);
	}
	
	@Test
	public void turningExecutionShouldReturnTheRoverPositionButFacedSud()throws MarsRoverException {
		String position = "";
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(0,1)");
		obstacles.add("(2,2)");
		
		MarsRover rover = new MarsRover(3,3,obstacles);
		
		position = rover.executeCommand("l");
		position = rover.executeCommand("l");
		assertEquals("(0,0,S)",position);
	}
	
	@Test
	public void turningExecutionShouldReturnTheRoverPositionButFacedNord()throws MarsRoverException {
		String position = "";
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(0,1)");
		obstacles.add("(2,2)");
		
		MarsRover rover = new MarsRover(3,3,obstacles);
		
		position = rover.executeCommand("r");
		position = rover.executeCommand("r");
		position = rover.executeCommand("r");
		position = rover.executeCommand("r");
		assertEquals("(0,0,N)",position);
	}
	
	@Test
	public void goForwardShouldReturnPositionWithXcoordPlusOne()throws MarsRoverException {
		String position = "";
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(0,1)");
		obstacles.add("(2,2)");
		
		MarsRover rover = new MarsRover(3,3,obstacles);
		
		position = rover.executeCommand("r");
		position = rover.executeCommand("f");
		assertEquals("(1,0,E)",position);
	}
	
	@Test
	public void goForwardShouldReturnPositionWithXcoordMinusOne()throws MarsRoverException {
		String position = "";
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(0,1)");
		obstacles.add("(2,2)");
		
		MarsRover rover = new MarsRover(3,3,obstacles);
		
		position = rover.executeCommand("r");
		position = rover.executeCommand("f");
		position = rover.executeCommand("b");
		assertEquals("(2,0,E)",position);
	}
	
	@Test
	public void combinedMovementShouldChangeRoverPositionToTwoTwoEst()throws MarsRoverException {
		String position = "";
		List<String> obstacles = null;
		
		MarsRover rover = new MarsRover(3,3,obstacles);
		
		position = rover.executeCommand("ffrff");
		assertEquals("(2,2,E)",position);
	}
	
	@Test
	public void backwardMovementShouldReturnZeroTwoNord()throws MarsRoverException {
		String position = "";
		List<String> obstacles = null;
		
		MarsRover rover = new MarsRover(3,3,obstacles);
		
		position = rover.executeCommand("b");
		assertEquals("(0,2,N)",position);
	}

}
